package server.controllers;

import data.networking.logging.ServerEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import server.Model;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {
    @FXML
    private ListView<ServerEvent> eventList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // La factory personalizzata permette di colorare in modo diverso le celle della ListView.
        eventList.setCellFactory(new Callback<>() {
            @Override
            public ListCell<ServerEvent> call(ListView<ServerEvent> param) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(ServerEvent item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setText(null);
                            setStyle("-fx-control-inner-background: derive(-fx-base,80%);");
                        } else {
                            setText(item.toString());
                            String style = switch (item.getType()) {
                                case SUCCESS -> "palegreen";
                                case ERROR -> "red";
                                case EXCEPTION -> "orangered";
                                case UNDEFINED -> "yellow";
                            };

                            setStyle("-fx-control-inner-background:derive(" + style + ",50%);");
                        }
                    }
                };
            }
        });

        eventList.setItems(Model.getInstance().getEvents().sorted());
    }

    @FXML
    private void handleEventListMouseClick(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            if (mouseEvent.getClickCount() == 2) {
                openNewEventViewerWindow(eventList.getSelectionModel().getSelectedItem());
            }
        }
    }

    private void openNewEventViewerWindow(ServerEvent selectedItem) {
        Stage stage = new Stage();

        FXMLLoader eventWindowLoader = new FXMLLoader(getClass().getResource("../structure/serverEventViewer.fxml"));
        Scene windowScene;

        try {
            windowScene = new Scene(eventWindowLoader.load());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        ServerEventController controller = eventWindowLoader.getController();
        controller.initialize(selectedItem);

        stage.setTitle(selectedItem.toString());

        stage.setScene(windowScene);
        stage.sizeToScene();
        stage.show();
    }
}
