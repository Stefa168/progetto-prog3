package server.controllers;

import data.networking.logging.Action;
import data.networking.logging.ServerEvent;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.text.SimpleDateFormat;

public class ServerEventController {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy 'alle' HH:mm.ss ");

    private ServerEvent serverEvent;

    @FXML
    private ListView<Action> actionsListView;
    @FXML
    private Label detailsLabel;

    public void initialize(ServerEvent serverEvent) {
        this.serverEvent = serverEvent;
        detailsLabel.setText(String.format("Richiesta di tipo %s inviata da %s il %s. Lo stato finale dell'evento è %s",
                                           serverEvent.getRequestType().toString(),
                                           serverEvent.getClientEmail(),
                                           DATE_FORMAT.format(serverEvent.getDate()),
                                           serverEvent.getType()));

        actionsListView.setCellFactory(new Callback<>() {
            @Override
            public ListCell<Action> call(ListView<Action> param) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(Action item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setText(null);
                            setStyle("-fx-control-inner-background: derive(-fx-base,80%);");
                        } else {
                            setText(item.toString());
                            String style = switch (item.getType()) {
                                case NORMAL -> "-fx-base,80%";
                                case ERROR -> "red,50%";
                                case EXCEPTION -> "orangered,50%";
                                case WARNING -> "yellow,50%";
                            };

                            setStyle("-fx-control-inner-background:derive(" + style + ");");
                        }
                    }
                };
            }
        });

        actionsListView.setItems(FXCollections.observableArrayList(serverEvent.getActions()));
    }
}
