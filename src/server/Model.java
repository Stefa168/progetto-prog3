package server;

import data.networking.logging.ServerEvent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {
    private static Model singleton;
    private ObservableList<ServerEvent> events = FXCollections.observableArrayList();

    public Model() {
        if (singleton != null) {
            throw new IllegalStateException("Model has already been instantiated");
        }
        singleton = this;
    }

    public static synchronized Model getInstance() {
        if (singleton == null) {
            throw new IllegalStateException("Model hasn't been initialized yet.");
        }
        return singleton;
    }

    public ObservableList<ServerEvent> getEvents() {
        return events;
    }

    public void addEvent(ServerEvent event) {
        Platform.runLater(() -> events.add(event));
    }
}
