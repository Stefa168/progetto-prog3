package server;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import server.concurrent.ConnectionListenerService;
import server.controllers.MainWindowController;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MailServer extends Application {
    public static final int SERVER_PORT = 34198;
    private static final int DEFAULT_MAX_CONNECTION_HANDLERS = 5;
    private static ServerSocket serverSocket;
    private static DataManager dataManager;
    Future<Void> connectionListener;
    private ExecutorService executorService;

    public static void main(String[] args) {
        launch(args);
    }

    public static void setSocketConnection(ServerSocket serverSocket) {
        Platform.runLater(() -> MailServer.serverSocket = serverSocket);
    }

    public static DataManager getDataManager() {
        return dataManager;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Map<String, String> runtimeParameters = getParameters().getNamed();

        if (!runtimeParameters.containsKey("dataPath")) {
            throw new IllegalArgumentException("Path to Emails Data Folder is required.");
        }

        dataManager = new DataManager(Paths.get(getParameters().getNamed().get("dataPath")));

        int MAX_CONNECTION_HANDLERS;
        if (!runtimeParameters.containsKey("maxHandlers")) {
            MAX_CONNECTION_HANDLERS = DEFAULT_MAX_CONNECTION_HANDLERS;
        } else {
            String handlers = runtimeParameters.get("maxHandlers");
            MAX_CONNECTION_HANDLERS = (handlers != null && Integer.parseInt(handlers) > 0) ?
                    Integer.parseInt(handlers) : DEFAULT_MAX_CONNECTION_HANDLERS;
        }

        new Model();

        executorService = Executors.newFixedThreadPool(MAX_CONNECTION_HANDLERS);

        connectionListener = executorService.submit(new ConnectionListenerService(executorService), null);

        initMainWindow();
    }

    private void initMainWindow() throws IOException {
        Stage stage = new Stage();

        FXMLLoader rootLoader = new FXMLLoader(getClass().getResource("structure/mainWindow.fxml"));
        Scene primaryScene = new Scene(rootLoader.load());

        MainWindowController controller = rootLoader.getController();

        // Se chiudiamo il programma, in automatico deve essere chiuso il server.
        stage.setOnCloseRequest((event) -> {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connectionListener.cancel(true);
            executorService.shutdown();
            Platform.exit();
        });

        stage.getIcons().add(new Image(MailServer.class.getResourceAsStream("server-icon.png")));
        stage.setTitle("Mail Server");

        stage.setScene(primaryScene);
        stage.sizeToScene();
        stage.show();
    }
}
