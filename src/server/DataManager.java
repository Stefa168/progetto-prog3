package server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import data.Email;
import data.PersistentDataContainer;
import utils.CloseableReentrantLock;
import utils.CloseableResource;
import utils.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

public class DataManager {
    private static final String EMAIL_EXTENSION = "json";
    private static final String PERSISTENT_DATA_FILENAME = "persistent.json";
    private final Path dataDirectory;
    private final Path persistentDataFilePath;

    private final Gson gson;

    private HashMap<String, CloseableReentrantLock> mailboxLockMap = new HashMap<>();

    public DataManager(Path dataDirectory) {
        if (Files.isRegularFile(dataDirectory)) {
            throw new IllegalArgumentException("Passed path is a File");
        }
        this.dataDirectory = dataDirectory;

        // Associamo le classi da serializzare con il loro adattatore.
        GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(PersistentDataContainer.class, new PersistentDataContainer.Adapter());
        gsonBuilder.registerTypeAdapter(Email.class, new Email.Adapter());
        gson = gsonBuilder.create();

        System.out.println(dataDirectory.toString());

        // controllo se la cartella dei dati non esiste
        if (!Files.isDirectory(dataDirectory)) {
            try {
                // e la creo.
                Files.createDirectory(dataDirectory);
            } catch (IOException e) {
                throw new RuntimeException("An error occurred while creating the server's data directory.", e);
            }
        }

        persistentDataFilePath = dataDirectory.resolve(PERSISTENT_DATA_FILENAME);

        // Se manca il file per i dati persistenti lo creo
        if (!Files.isRegularFile(persistentDataFilePath)) {
            try {
                Files.createFile(persistentDataFilePath);
            } catch (IOException e) {
                throw new RuntimeException("An error occurred while creating the server's data file.", e);
            }

            try (FileWriter fileWriter = new FileWriter(persistentDataFilePath.toFile());
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                 JsonWriter writer = new JsonWriter(bufferedWriter)) {
                gson.toJson(new PersistentDataContainer(), PersistentDataContainer.class, writer);
            } catch (IOException e) {
                throw new RuntimeException("An error occurred while initializing the server's data file.", e);
            }
        }
    }

    /**
     * Il metodo restituisce il lock specifico di una email, previa creazione se non esiste.
     * I lock delle email sono necessari per permettere di eseguire in parallelo alcune operazioni su email separate.
     * Con il metodo impostato come synchronized, è possibile recuperare solo un lock specifico di una mail alla
     * volta in modo da non avere sovrascrizioni o duplicati.
     *
     * @param email L'email per la quale dobbiamo abilitare la mutua esclusione.
     * @return Una {@link CloseableResource} utilizzabile in un try-with-resources, dato che estende
     * {@link AutoCloseable}.
     * @throws IllegalArgumentException Vedere {@link StringUtils#sanitizeEmail(String)}.
     */
    private synchronized CloseableResource getEmailLock(String email) throws IllegalArgumentException {
        CloseableReentrantLock emailLock = mailboxLockMap.get(StringUtils.sanitizeEmail(email));
        if (emailLock == null) {
            emailLock = new CloseableReentrantLock();
            mailboxLockMap.put(email, emailLock);
        }
        return emailLock.lockAsResource();
    }

    /**
     * Verifica se esiste una cartella dedicata all'utente specificato.
     *
     * @param email Il nome della mail dell'utente richiesto
     * @throws IllegalArgumentException Vedere {@link StringUtils#sanitizeEmail(String)}.
     */
    public boolean userExists(String email) throws IllegalArgumentException {
        Path pathToEmail = dataDirectory.resolve(StringUtils.sanitizeEmail(email));
        return Files.isDirectory(pathToEmail);
    }


    /**
     * Recupera tutte le Email di un utente che non corrispondono a quelle già possedute.
     * Nella struttura dati, il nome dell'email coincide con il nome della cartella che contiene i dati delle email.
     * Ogni email ha un suo file con, come nome, l'ID della mail stessa, e come estensione ".email".
     *
     * @param email                Il nome della mail dell'utente richiesto
     * @param alreadyOwnedEmailIDs Set (preferibilmente HashSet per accelerare i controlli) contenente tutti gli ID
     *                             delle email che il client ha già in locale.
     * @throws IllegalArgumentException Vedere {@link StringUtils#sanitizeEmail(String)}.
     * @implNote Per evitare inconsistenze durante il recupero dei dati, è necessario utilizzare dei lock per ogni
     * casella. Inoltre, se il server è stato appena avviato, i lock sopra citati saranno creati solo alla prima
     * richiesta dei dati di una email. Per evitare che un lock sia creato più volte il metodo {@link #registerEmail}
     * è synchronized.
     */
    public Set<Email> getMissingUserEmails(String email, Set<Integer> alreadyOwnedEmailIDs) throws IllegalArgumentException {
        Set<Email> newEmails = new HashSet<>();

        // Seconda sezione critica: ora che ho il lock della casella, cerco di recuperare i dati.
        //noinspection unused
        try (CloseableResource lock = getEmailLock(email);
             Stream<Path> emailPaths = Files.walk(dataDirectory.resolve(email))) {
            Iterator<Path> pathIterator;
            pathIterator = emailPaths.filter(Files::isRegularFile)
                                     // Questo filtro serve per escludere dai risultati tutte le email che sono già
                                     // possedute dal client.
                                     .filter(path -> {
                                         if (alreadyOwnedEmailIDs == null || alreadyOwnedEmailIDs.size() == 0) {
                                             return true;
                                         }
                                         // Per qualche ragione ignota, toString sulla Path non fa proseguire la lambda.
                                         String emailID = String.valueOf(path.getFileName());
                                         // Otteniamo l'ID della mail
                                         emailID = emailID.substring(0, emailID.lastIndexOf('.'));
                                         return !alreadyOwnedEmailIDs.contains(Integer.parseInt(emailID));
                                     }).iterator();

            // Ora mappiamo tutte le path con le effettive email, recuperando i dati dai file relativi.
            // Viene utilizzato un iterator per non dover lanciare eccezioni non intercettabili.
            while (pathIterator.hasNext()) {
                try (FileReader fileReader = new FileReader(pathIterator.next().toFile());
                     BufferedReader bufferedReader = new BufferedReader(fileReader);
                     JsonReader reader = new JsonReader(bufferedReader)) {
                    Email readEmail = gson.fromJson(reader, Email.class);
                    newEmails.add(readEmail);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return newEmails;
    }

    /**
     * Registra una email nel database.
     *
     * @param email L'email da registrare
     * @throws IllegalArgumentException Se la mail è già stata registrata nel sistema, altrimenti nel caso in cui la
     *                                  mail sia invalida, vedere {@link StringUtils#sanitizeEmail(String)} .
     */
    public void registerEmail(String email) throws IllegalArgumentException, IOException {
        //noinspection unused
        try (CloseableResource emailLock = getEmailLock(email)) {
            if (userExists(email)) {
                throw new IllegalArgumentException(String.format("Email \"%s\" has been already registered.", email));
            }

            String safeEmail = StringUtils.sanitizeEmail(email);

            try {
                Files.createDirectory(dataDirectory.resolve(safeEmail));
            } catch (IOException e) {
                throw new IOException(String.format("IO Error while registering email \"%s\"", safeEmail), e);
            }
        }
    }

    /**
     * Scrive a file, nella directory di un utente, la mail indicata.
     *
     * @param recipient Il nome della casella in cui salvare la mail come file JSON
     * @param email     La mail da salvare
     */
    public void deliverEmailToRecipient(String recipient, Email email) {
        if (!userExists(recipient)) {
            throw new IllegalArgumentException("Specified Recipient does not exist (" + recipient + ")");
        }

        //noinspection unused
        try (CloseableResource emailLock = getEmailLock(recipient)) {
            File newEmailPath = dataDirectory.resolve(recipient)
                                             .resolve(String.format("%d.%s", email.getId(), EMAIL_EXTENSION))
                                             .toFile();

            try (FileWriter fileWriter = new FileWriter(newEmailPath, false);
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                 JsonWriter writer = new JsonWriter(bufferedWriter)) {
                gson.toJson(email, Email.class, writer);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Cancella il file di una casella di posta
     *
     * @param owner   L'email della casella di posta
     * @param emailID L'ID della email da rimuovere
     * @return true se l'email è stata cancellata (e quindi esisteva), false altrimenti
     */
    public boolean deleteEmail(String owner, int emailID) {
        if (!userExists(owner)) {
            throw new IllegalArgumentException("Specified Owner does not exist (" + owner + ")");
        }

        File emailToDeletePath = dataDirectory.resolve(owner)
                                              .resolve(String.format("%d.%s", emailID, EMAIL_EXTENSION))
                                              .toFile();
        //noinspection unused
        try (CloseableResource emailLock = getEmailLock(owner)) {
            return emailToDeletePath.delete();
        }
    }

    /**
     * Riserva un ID per una mail.
     *
     * @return L'ID generato.
     */
    public synchronized int reserveEmailID() {
        PersistentDataContainer container;

        try (FileReader fileReader = new FileReader(persistentDataFilePath.toFile());
             BufferedReader bufferedReader = new BufferedReader(fileReader);
             JsonReader reader = new JsonReader(bufferedReader)) {
            container = gson.fromJson(reader, PersistentDataContainer.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int id = container.getIncrementID();

        try (FileWriter fileWriter = new FileWriter(persistentDataFilePath.toFile());
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             JsonWriter writer = new JsonWriter(bufferedWriter)) {
            gson.toJson(container, PersistentDataContainer.class, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return id;
    }
}
