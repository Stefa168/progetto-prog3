package server.concurrent;

import server.MailServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;
import java.util.concurrent.ExecutorService;

import static server.MailServer.SERVER_PORT;

public class ConnectionListenerService implements Runnable {
    ExecutorService executorService;

    public ConnectionListenerService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public void run() {
        // https://stackoverflow.com/questions/56112598/proper-way-to-close-an-autocloseable
        // A quanto pare non è completamente corretto utilizzare finally per chiudere il socket perchè
        // ci sono dei casi particolari in cui non avviene quella sezione di codice.
        // Il socket è quindi creato con un try with resources; in questo modo vengono sempre chiuse
        // le risorse quanto il try fallisce.
        try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
            System.out.println("Connection Open. Server listening on port " + SERVER_PORT);

            MailServer.setSocketConnection(serverSocket);

            while (!serverSocket.isClosed()) {
                try {
                    Socket connection = serverSocket.accept();

                    System.out.println("\n" + new Date() + " Received Connection:\n" + connection.toString());

                    ConnectionHandler handler = new ConnectionHandler(connection);
                    executorService.submit(handler);
                } catch (SocketException e) {
                    if (e.getMessage().equals("Socket closed")) {
                        System.out.println("Socket chiuso.");
                    } else {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
