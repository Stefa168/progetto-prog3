package server.concurrent;

import data.Email;
import data.networking.answer.*;
import data.networking.logging.ServerEvent;
import data.networking.request.*;
import server.MailServer;
import server.Model;
import utils.CloseableResource;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static utils.StringUtils.*;

/**
 * Il ConnectionHandler è un Runnable lanciato dal server nel momento in cui riceve una connessione in entrata, per
 * permettere la risposta in parallelo a più richieste di multipli Client. E' questa classe ad occuparsi dei diversi
 * tipi di richieste client, e di chiudere il socket ricevuto.
 */
public class ConnectionHandler implements Runnable {
    private final Socket connection;

    public ConnectionHandler(Socket connection) {
        this.connection = connection;
    }

    @Override
    public void run() {
        ServerEvent event = new ServerEvent();
        event.addNormal("Ricevuta connessione da " + connection.getInetAddress().toString());

        //noinspection unused
        try (CloseableResource toClose = event::conclude) {
            try (ObjectInputStream inputStream = new ObjectInputStream(connection.getInputStream());
                 ObjectOutputStream outputStream = new ObjectOutputStream(connection.getOutputStream())) {

                // Teniamo la risposta del server generica per poter anche inviare, se necessario, un
                // errore al client. Nel caso in cui la registrazione o il recupero dei dati fallissero,
                // restituiamo comunque un errore generico per non far lanciare eccezioni lato client.
                ServerAnswer answer = new ErrorAnswer("Errore lato server");

                try {
                    ClientRequest clientRequest = (ClientRequest) inputStream.readObject();

                    event.setRequest(clientRequest);

                    System.out.println(clientRequest);

                    // Differenziamo il tipo di richiesta in funzione del tipo che otteniamo in risposta.
                    switch (clientRequest.getType()) {
                        case PULL -> {
                            PullRequest pullRequest = (PullRequest) clientRequest;

                            Set<Email> newEmails = MailServer.getDataManager()
                                                             .getMissingUserEmails(pullRequest.getEmail(),
                                                                                   pullRequest.getLocalEmailIDs());

                            answer = new NewEmailsAnswer(newEmails);

                            event.addNormal(String.format("Recuperate %d nuove email mancanti", newEmails.size()));

                            System.out.println(answer);

                            outputStream.writeObject(answer);
                        }
                        case CHECK -> {
                            CheckRequest checkRequest = (CheckRequest) clientRequest;

                            HashMap<String, Boolean> existenceMap = null;
                            // Creo la mappa delle esistenze.
                            existenceMap = checkRequest.getEmailsToCheck()
                                                       .stream()
                                                       // Questo collettore mappa la coppia (email, Boolean)
                                                       // nella HashMap.
                                                       .collect(Collectors.toMap(email -> email,
                                                                                 email -> MailServer.getDataManager()
                                                                                                    .userExists(email),
                                                                                 (a, b) -> b,
                                                                                 HashMap::new));

                            answer = new EmailExistenceAnswer(existenceMap);

                            event.addNormal(String.format("Delle %d email specificate, %d non esistono.",
                                                          checkRequest.getEmailsToCheck().size(),
                                                          existenceMap.keySet()
                                                                      .stream()
                                                                      .map(existenceMap::get)
                                                                      .filter(v -> !v)
                                                                      .count()));

                            outputStream.writeObject(answer);
                        }
                        case AUTH -> {
                            AuthRequest authRequest = (AuthRequest) clientRequest;

                            try {
                                // Se l'utente non è registrato nel database...
                                if (!MailServer.getDataManager().userExists(authRequest.getEmail())) {
                                    event.addNormal("L'email specificata non è registrata nel database.");
                                    // Provo a registrare l'email
                                    MailServer.getDataManager().registerEmail(authRequest.getEmail());
                                    answer = new NewEmailsAnswer(new HashSet<>(), "Email registrata con successo");
                                    event.addNormal("Email registrata con successo");
                                } else {
                                    // Altrimenti esiste già, per cui recuperiamo tutte le email che deve avere nella
                                    // casella subito dopo l'avvio.
                                    Set<Email> emails = MailServer.getDataManager()
                                                                  .getMissingUserEmails(authRequest.getEmail(),
                                                                                        new HashSet<>());

                                    answer = new NewEmailsAnswer(emails);
                                    event.addNormal("L'email è stata già registrata; recupero le email...");
                                }
                            } catch (IOException e) {
                                answer = new ErrorAnswer(e.getMessage());
                                event.addError(e.getMessage());
                                e.printStackTrace();
                            }
                        }
                        case SEND -> {
                            SendRequest sendRequest = (SendRequest) clientRequest;

                            SendAnswer sendAnswer = null;

                            // Recuperiamo le email ripulite.
                            SanitizationResults recipients = sanitizeEmailListFromString(sendRequest.getEmailObject()
                                                                                                    .getTo());

                            try {
                                // Cerchiamo di ripulire il mittente
                                sanitizeEmail(sendRequest.getEmailObject().getFrom());
                            } catch (IllegalArgumentException e) {
                                // Se siamo qui, vuol dire che c'è stato un problema con la sanitizzazione.
                                // Le stringhe restituite con l'errore sono in inglese, tuttavia gli errori non
                                // possono avvenire se tutto viene eseguito come previsto (cioè dal client e non da
                                // terzi malevoli)
                                sendAnswer = new SendAnswer(false, e.getMessage());
                                event.addError(e.getMessage());
                            } catch (Exception e) {
                                e.printStackTrace();
                                event.addException(e);
                            }

                            // Se non ci sono stati problemi
                            if (sendAnswer == null) {
                                // e tutte le email sono a posto
                                if (recipients.areEmailsGood()) {
                                    int reservedID = MailServer.getDataManager().reserveEmailID();

                                    Email emailToSend = sendRequest.getEmailObject();
                                    emailToSend.setId(reservedID);

                                    recipients.getEmails(true)
                                              .forEach(recipient -> MailServer.getDataManager()
                                                                              .deliverEmailToRecipient(recipient,
                                                                                                       emailToSend));

                                    answer = new SendAnswer(true, "OK");
                                    event.addNormal("Email recapitata a tutte le caselle specificate.");
                                } else {
                                    answer = new SendAnswer(false,
                                                            "Queste email non sono valide: \n" + recipients.toString(false));
                                    event.addNormal("Alcune email specificate non sono valide:\n" + recipients.toString(false));
                                }
                            }
                        }
                        case DELETE -> {
                            DeleteRequest deleteRequest = (DeleteRequest) clientRequest;
                            String clientEmail = deleteRequest.getEmail();

                            if (MailServer.getDataManager().userExists(clientEmail)) {
                                event.addNormal(String.format("L'email %s esiste.", clientEmail));

                                boolean success = true;
                                StringBuilder missingEmails = new StringBuilder("[");
                                for (int id : deleteRequest.getEmailIDs()) {
                                    boolean opResult = MailServer.getDataManager().deleteEmail(clientEmail, id);
                                    if (!opResult) {
                                        success = false;
                                        missingEmails.append(id).append(" ");
                                    }
                                }
                                missingEmails.append(']');
                                answer = new DeleteAnswer(success, missingEmails.toString());

                                event.addWarning("Delle email da eliminare, " + (missingEmails.length() - 2) +
                                                 " erano mancanti.");

                            } else {
                                answer = new ErrorAnswer(String.format("L'email %s non esiste.", clientEmail));
                                event.addError(String.format("L'email %s non esiste.", clientEmail));
                            }
                        }
                        default -> {
                            System.err.printf("Request of type %s not supported.\n", clientRequest.getType());
                            answer = new ErrorAnswer(String.format(
                                    "Request of type %s not supported.", clientRequest.getType()));
                            event.addError("Il tipo di richiesta ricevuto non è supportato.");
                        }
                    }

                } catch (IllegalArgumentException e) {
                    System.err.println(e.getMessage());
                    event.addException(e);
                    answer = new ErrorAnswer(e.getMessage());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    event.addException(e);
                } finally {
                    outputStream.writeObject(answer);
                    // event.addAction("Inviata risposta: \n" + answer.toString(), Action.MessageType.NORMAL);
                }
            } catch (IOException e) {
                e.printStackTrace();
                event.addException(e);
            } finally {
                connection.close();
                event.addNormal("Connessione chiusa.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            event.addException(e);
        }

        Model.getInstance().addEvent(event);
    }
}
