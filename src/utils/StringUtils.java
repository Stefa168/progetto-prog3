package utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringUtils {
    /**
     * Verifica se l'email passata è utilizzabile oppure no.
     *
     * @param email L'email da controllare
     * @return Se l'email è accettabile viene restituita, previa rimozione di spazi e caratteri che non le appartengono.
     * @throws IllegalArgumentException Se la stringa che codifica l'email è null/vuota o se è malformata.
     * @implNote Il metodo quando esegue il matching, restituirà solo la prima email identificata, mentre le altre
     * saranno scartate. La email è tutta impostata lower case.
     * Per sanitizzare multiple mail, utilizzare {@link #sanitizeEmailListFromString(String)}
     */
    public static String sanitizeEmail(String email) throws IllegalArgumentException {
        if (email == null) {
            throw new IllegalArgumentException("An email cannot be null");
        } else if (email.isEmpty()) {
            throw new IllegalArgumentException("An email cannot be empty");
        } else {
            Pattern simpleEmailPattern = Pattern.compile("[^@]+@[^.]+\\..+");
            Matcher matcher = simpleEmailPattern.matcher(email);
            if (matcher.find()) {
                return matcher.group().toLowerCase();
            } else {
                throw new IllegalArgumentException(email + ": Malformed Email");
            }
        }
    }

    public static SanitizationResults sanitizeEmailListFromString(String emailList) throws IllegalArgumentException {
        if (emailList == null) {
            throw new IllegalArgumentException("Email list string is null");
        } else if (emailList.isEmpty()) {
            throw new IllegalArgumentException("Email list string is empty");
        } else {
            // Splittiamo tutte le email nella lista
            String[] emails = emailList.toLowerCase().split(",");

            HashMap<String, Boolean> validationResults = new HashMap<>();
            boolean allEmailsAreGood = true;

            Pattern simpleEmailPattern = Pattern.compile("[A-Za-z0-9_.-]+@.+[A-Za-z0-9_-]+");

            // Per ogni stringa che potrebbe contenere UNA email
            for (String email : emails) {
                Matcher matcher = simpleEmailPattern.matcher(email);
                // Vedo se trovo una email
                boolean found = matcher.find();
                // Salvo la mail ripulita se l'ho trovata, altrimenti la stringa che dovrebbe contenerla;
                // il valore associato indica se la mail è OK o no.
                validationResults.put(found ? matcher.group() : email, found);
                // Se la mail che potrebbe esserci non è OK, mi appunto il problema.
                if (!found) {
                    allEmailsAreGood = false;
                }
            }

            return new SanitizationResults(allEmailsAreGood, validationResults);
        }
    }

    /**
     * Classe che codifica i risultati della pulizia delle email.
     * Contiene una HashMap che codifica coppie ({@link String}, {@link Boolean}), dove la stringa codifica una email
     * e il valore booleano indica se l'email è accettabile oppure no. <br>
     * E' inoltre presente un boolean che permette di sapere se tutte le email sono accettabili o no, senza quindi
     * dover iterare l'intera HashMap.
     */
    public static class SanitizationResults {
        final boolean allEmailsAreGood;
        final HashMap<String, Boolean> validationResults;

        public SanitizationResults(boolean allEmailsAreGood, HashMap<String, Boolean> validationResults) {
            this.allEmailsAreGood = allEmailsAreGood;
            this.validationResults = validationResults;
        }

        public boolean areEmailsGood() {
            return allEmailsAreGood;
        }

        public Set<String> getEmails(boolean good) {

            return validationResults.keySet()
                                    .stream()
                                    .filter(email -> validationResults.get(email) == good)
                                    .collect(Collectors.toCollection(HashSet::new));
        }

        public String toString(boolean good) {
            return validationResults.keySet()
                                    .stream()
                                    .filter(email -> validationResults.get(email) == good)
                                    .map(recipient -> "\t- " + recipient + '\n')
                                    .collect(Collectors.joining());
        }
    }
}
