package utils;

import java.util.concurrent.locks.ReentrantLock;

public class CloseableReentrantLock extends ReentrantLock {

    /**
     * @return un {@link AutoCloseable} una volta che il lock è stato acquisito.
     */
    public CloseableResource lockAsResource() {
        lock();
        return this::unlock;
    }
}