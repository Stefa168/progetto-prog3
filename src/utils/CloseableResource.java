package utils;

public interface CloseableResource extends AutoCloseable {

    @Override
    void close();
}
