package data;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.Serializable;

public class PersistentDataContainer implements Serializable {
    private int emailIDCounter;

    public PersistentDataContainer() {
        this.emailIDCounter = 0;
    }

    private PersistentDataContainer(int startValue) {this.emailIDCounter = startValue;}

    public synchronized int getIncrementID() {
        return emailIDCounter++;
    }

    public static class Adapter extends TypeAdapter<PersistentDataContainer> {

        @Override
        public void write(JsonWriter jsonWriter, PersistentDataContainer persistentDataContainer) throws IOException {
            jsonWriter.beginObject();
            jsonWriter.name("emailIDCounter");
            jsonWriter.value(persistentDataContainer.emailIDCounter);
            jsonWriter.endObject();
        }

        @Override
        public PersistentDataContainer read(JsonReader jsonReader) throws IOException {
            jsonReader.beginObject();
            jsonReader.nextName();
            PersistentDataContainer container = new PersistentDataContainer(jsonReader.nextInt());
            jsonReader.endObject();
            return container;
        }
    }
}
