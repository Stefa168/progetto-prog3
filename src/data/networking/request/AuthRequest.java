package data.networking.request;

/**
 * La classe AuthRequest è utilizzata per dichiarare l'intenzione di un client di accedere ad una casella di posta.
 * Se l'email non è mai stata registrata, lo sarà, altrimenti vengono restituite al client tutte le email della casella.
 */
public class AuthRequest extends ClientRequest {
    public AuthRequest(String email) {
        super(email);
    }

    @Override
    public Type getType() {
        return Type.AUTH;
    }

    @Override
    public String toString() {
        return String.format("AuthRequest: %s", email);
    }
}
