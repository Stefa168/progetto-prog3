package data.networking.request;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * La Pull Request è utilizzata per trasmettere al server la lista delle email già ricevute.
 * In questo modo è possibile restituire al client solo le email effettivamente mancanti in locale.
 */
public class PullRequest extends ClientRequest {

    private final Set<Integer> localEmailIDs;

    public PullRequest(String email, Set<Integer> localEmailIDs) {
        super(email);
        this.localEmailIDs = localEmailIDs;
    }

    @Override
    public Type getType() {
        return Type.PULL;
    }

    public Set<Integer> getLocalEmailIDs() {
        return localEmailIDs;
    }

    @Override
    public String toString() {
        String emails = localEmailIDs.stream()
                                     .map(Object::toString)
                                     .collect(Collectors.joining(", ", "[", "]"));

        return String.format("""
                                     PullRequest {
                                     \tClient email: %s
                                     \tLocal email IDs: %s
                                     }""", email, emails);
    }
}
