package data.networking.request;

import data.Email;

public class SendRequest extends ClientRequest {

    private final Email email;

    public SendRequest(Email email) {
        super(email.getFrom());
        this.email = email;
    }

    public Email getEmailObject() {
        return email;
    }

    @Override
    public Type getType() {
        return Type.SEND;
    }

    @Override
    public String toString() {
        return "SendRequest:\n" + email;
    }
}
