package data.networking.request;

import data.Email;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class DeleteRequest extends ClientRequest {
    private final Set<Integer> emailIDsToDelete;

    public DeleteRequest(String email, Set<Email> emailsToDelete) {
        super(email);
        this.emailIDsToDelete = emailsToDelete.stream()
                                              .map(Email::getId)
                                              .collect(Collectors.toUnmodifiableSet());
    }

    public Set<Integer> getEmailIDs() {
        return emailIDsToDelete;
    }

    @Override
    public Type getType() {
        return Type.DELETE;
    }

    @Override
    public String toString() {
        return emailIDsToDelete.stream()
                               .map(Objects::toString)
                               .collect(Collectors.joining(", ", "DeleteRequest: [", "]"));
    }
}
