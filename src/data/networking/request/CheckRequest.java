package data.networking.request;

import java.util.HashSet;
import java.util.Set;

/**
 * Questa classe identifica un messaggio inviato da un client che richiede informazioni al server riguardo
 * l'esistenza delle email di un set.
 */
public class CheckRequest extends ClientRequest {

    private final Set<String> emailsToCheck;

    public CheckRequest(String email) {
        super(email);
        emailsToCheck = new HashSet<>();
        emailsToCheck.add(email);
    }

    public CheckRequest(String email, Set<String> emailsToCheck) {
        super(email);
        this.emailsToCheck = emailsToCheck;
    }

    public Set<String> getEmailsToCheck() {
        return emailsToCheck;
    }

    @Override
    public Type getType() {
        return Type.CHECK;
    }

    @Override
    public String toString() {
        return String.format("CheckRequest {\n\tEmail Client: %s\n}", email);
    }
}
