package data.networking.request;

import java.io.Serializable;
import java.security.InvalidParameterException;

/**
 * La classe ClientRequest è una classe generica, pensata per le richieste del client.
 * Deve essere per forza estesa, essendo astratta.
 * Per distinguere comodamente (ad esempio in uno switch) il tipo di richiesta specifico è possibile utilizzare il
 * metodo {@link #getType()}, esteso da ogni richiesta.
 */
public abstract class ClientRequest implements Serializable {

    protected final String email;

    ClientRequest(String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidParameterException("Email cannot be null or empty.");
        }

        this.email = email;
    }

    /**
     * @return Il nome della mail, come stringa.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return Il tipo di richiesta che la classe identifica.
     */
    public abstract Type getType();

    // Il metodo toString è stato reso astratto per richiedere che le classi figlie abbiano una loro versione apposita.
    @Override
    public abstract String toString();

    /**
     * L'enumeratore Type codifica tutti i generi di richieste esistenti e accettabili.
     * <p>
     *     <ul>
     *          <li>CHECK è utilizzato per richiedere al server di verificare se una mail esiste o meno</li>
     *          <li>AUTH è utilizzato per accedere ad una casella specifica, recuperando tutti i dati specifici</li>
     *          <li>PULL è utilizzato per richiedere al server tutte le nuove email non possedute localmente</li>
     *          <li>SEND è utilizzato per inviare al server una mail</li>
     *          <li>DELETE è utilizzato per inviare un comando di cancellazione di una mail al server</li>
     *          <li>UNDEFINED è per casi rari o per testing</li>
     *   </ul>
     * </p>
     */
    public enum Type {
        CHECK,
        AUTH,
        PULL,
        SEND,
        DELETE,
        UNDEFINED
    }
}
