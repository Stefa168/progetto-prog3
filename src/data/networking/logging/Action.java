package data.networking.logging;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Action {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("H:m.s ");
    private final String message;
    private final MessageType type;


    public Action(String message, MessageType type) {
        this.message = dateFormat.format(new Date()) + message;
        this.type = type;
    }

    @Override
    public String toString() {
        return message;
    }

    public MessageType getType() {
        return type;
    }

    public enum MessageType {
        NORMAL,
        WARNING,
        ERROR,
        EXCEPTION
    }
}
