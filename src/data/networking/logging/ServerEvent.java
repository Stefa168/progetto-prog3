package data.networking.logging;

import data.networking.request.ClientRequest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ServerEvent implements Comparable<ServerEvent> {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm.ss ");
    private String clientEmail = null;
    private ClientRequest.Type requestType;
    private Type type;
    private List<Action> actions;
    private Date date;
    private boolean concluded = false;

    public ServerEvent() {
        actions = new ArrayList<>();
        type = Type.UNDEFINED;
        this.date = new Date();
    }

    private void throwIfConcluded() {
        if (concluded)
            throw new IllegalStateException("ServerEvent has been closed");
    }

    public void setRequest(ClientRequest request) {
        if (clientEmail == null) {
            clientEmail = request.getEmail();
            requestType = request.getType();
            addNormal(String.format("La richiesta è di tipo %s e proviene da %s.", requestType.toString(),
                                    clientEmail));
        } else {
            throw new IllegalStateException("ServerEvent request has already been set");
        }

    }

    public String getClientEmail() {
        return clientEmail;
    }

    public ClientRequest.Type getRequestType() {
        return requestType;
    }

    public Type getType() {
        return type;
    }

    public List<Action> getActions() {
        return Collections.unmodifiableList(actions);
    }

    public Date getDate() {
        return date;
    }

    public boolean isConcluded() {
        return concluded;
    }

    public synchronized void conclude() {
        if (!concluded) {
            if (type == Type.UNDEFINED) {
                type = Type.SUCCESS;
            }
        }
        concluded = true;
    }

    public void addNormal(String message) {
        throwIfConcluded();
        actions.add(new Action(message, Action.MessageType.NORMAL));
    }

    public void addWarning(String message) {
        throwIfConcluded();
        actions.add(new Action(message, Action.MessageType.WARNING));
    }

    public void addError(String message) {
        throwIfConcluded();
        actions.add(new Action(message, Action.MessageType.ERROR));
        type = Type.ERROR;
    }

    public void addException(Exception e) {
        throwIfConcluded();
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);

        actions.add(new Action(sw.toString(), Action.MessageType.EXCEPTION));
        type = Type.EXCEPTION;
    }

    @Override
    public String toString() {
        return String.format("%s%s %s", dateFormat.format(date), clientEmail, requestType.toString());
    }

    @Override
    public int compareTo(ServerEvent o) {
        return date.compareTo(o.date);
    }

    public enum Type {
        SUCCESS,
        ERROR,
        EXCEPTION,
        UNDEFINED
    }

}
