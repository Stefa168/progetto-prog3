package data.networking.answer;

import static data.networking.answer.ServerAnswer.Type.DELETE;

public class DeleteAnswer extends ServerAnswer {

    public DeleteAnswer(boolean success, String statusMessage) {
        super(success, statusMessage);
    }

    @Override
    public Type getType() {
        return DELETE;
    }

    @Override
    public String toString() {
        return String.format("DeleteAnswer: %b, %s", success, statusMessage);
    }
}
