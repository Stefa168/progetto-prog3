package data.networking.answer;

import java.io.Serializable;

public abstract class ServerAnswer implements Serializable {
    final boolean success;
    final String statusMessage;

    ServerAnswer(boolean success, String statusMessage) {
        this.success = success;
        this.statusMessage = statusMessage;
    }

    public abstract Type getType();

    public boolean isSuccess() {
        return success;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    @Override
    public abstract String toString();

    public enum Type {
        DELETE,
        EMAIL_EXISTENCE,
        ERROR,
        NEW_EMAILS,
        SEND_RESULT,
        UNDEFINED
    }
}