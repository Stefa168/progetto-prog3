package data.networking.answer;

import java.util.HashMap;
import java.util.stream.Collectors;

public class EmailExistenceAnswer extends ServerAnswer {

    private final HashMap<String, Boolean> results;

    public EmailExistenceAnswer(HashMap<String, Boolean> results) {
        super(true, "");
        this.results = results;
    }

    public HashMap<String, Boolean> getExistenceResults() {
        return results;
    }

    public boolean getExistenceResult(String email) throws IllegalArgumentException {
        if (results.containsKey(email)) {
            return results.get(email);
        } else {
            throw new IllegalArgumentException(String.format("Email \"%s\" isn't part of original query.", email));
        }
    }

    @Override
    public Type getType() {
        return Type.EMAIL_EXISTENCE;
    }

    @Override
    public String toString() {
        if (!success) {
            return String.format("EmailExistenceAnswer: %s\n", statusMessage);
        } else {
            String builder = results.keySet()
                                    .stream()
                                    .map(email -> String.format("\t(%s %b)", email, results.get(email)))
                                    .collect(Collectors.joining());

            return String.format("EmailExistenceAnswer {\n%s\n}", builder);
        }
    }
}
