package data.networking.answer;

import data.Email;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class NewEmailsAnswer extends ServerAnswer {
    private final Set<Email> emails;

    public NewEmailsAnswer(Set<Email> emails) {
        super(true, "OK");
        this.emails = emails;
    }

    public NewEmailsAnswer(Set<Email> emails, String statusMessage) {
        super(true, statusMessage);
        this.emails = emails;
    }

    /**
     * Intesa per errori e fallimenti; usare {@link ErrorAnswer} al suo posto.
     *
     * @param statusMessage
     */
    @Deprecated
    public NewEmailsAnswer(String statusMessage) {
        super(false, statusMessage);
        this.emails = null;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    @Override
    public Type getType() {
        return Type.NEW_EMAILS;
    }

    @Override
    public String toString() {
        if (!success || emails == null) {
            return String.format("NewEmailsAnswer: %s\n", statusMessage);
        } else {
            String emailsString = emails.stream()
                                        // alternativamente
                                        // .map(Email::toString)
                                        .map(email -> Arrays.stream(email.toString()
                                                                         .split("\n"))
                                                            .collect(Collectors.joining("\n\t", "\t", "\n")))
                                        .collect(Collectors.joining("-------------\n"));

            return String.format("""
                                         NewEmailsAnswer {
                                         Status: %s
                                         Emails:
                                         %s
                                         }""",
                                 statusMessage,
                                 emailsString);
        }
    }
}
