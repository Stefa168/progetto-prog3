package data.networking.answer;

public class SendAnswer extends ServerAnswer {

    public SendAnswer(boolean success, String statusMessage) {
        super(success, statusMessage);
    }

    @Override
    public Type getType() {
        return Type.SEND_RESULT;
    }

    @Override
    public String toString() {
        return String.format("SendAnswer: %s; Status: %s", success ? "Succeeded" : "Failed", statusMessage);
    }
}
