package data.networking.answer;

public class ErrorAnswer extends ServerAnswer {
    public ErrorAnswer(String statusMessage) {
        super(false, statusMessage);
    }

    @Override
    public Type getType() {
        return Type.ERROR;
    }

    @Override
    public String toString() {
        return "ErrorAnswer: " + statusMessage;
    }


}
