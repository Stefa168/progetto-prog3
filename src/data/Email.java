package data;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Email implements Serializable, Comparable<Email> {
    public transient static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("'Inviata' EEEE d MMMM yyyy 'alle' H:m");

    private transient SimpleIntegerProperty id;
    private transient SimpleStringProperty from;
    private transient SimpleStringProperty to;
    private transient SimpleStringProperty subject;
    private transient SimpleStringProperty text;
    private transient SimpleObjectProperty<Date> dateSent;

    // Costruttore utilizzabile per la serializzazione con JSON.
    private Email() {}

    public Email(int id, String from, String to, String subject, String text, Date dateSent) {
        this.id = new SimpleIntegerProperty(id);
        this.from = new SimpleStringProperty(from);
        this.to = new SimpleStringProperty(to);
        this.subject = new SimpleStringProperty(subject);
        this.text = new SimpleStringProperty(text);
        this.dateSent = new SimpleObjectProperty<>(dateSent);
    }

    // Funzioni autogenerate
    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public String getFrom() {
        return from.get();
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public SimpleStringProperty fromProperty() {
        return from;
    }

    public String getTo() {
        return to.get();
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public SimpleStringProperty toProperty() {
        return to;
    }

    public String getSubject() {
        return subject.get();
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public SimpleStringProperty subjectProperty() {
        return subject;
    }

    public String getText() {
        return text.get();
    }

    public void setText(String text) {
        this.text.set(text);
    }

    public SimpleStringProperty textProperty() {
        return text;
    }

    public Date getDateSent() {
        return dateSent.get();
    }

    public void setDateSent(Date dateSent) {
        this.dateSent.set(dateSent);
    }

    public SimpleObjectProperty<Date> dateSentProperty() {
        return dateSent;
    }

    @Override
    public String toString() {
        return String.format("Email con ID %d\nOggetto: %s\nInviata da %s a %s\n%s\n", id.get(),
                             subject.get(), from.get(), to.get(), formattedDate());
    }

    public String formattedDate() {
        return dateFormat.format(dateSent.get());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return id.get() == email.id.get();
    }

    // Per serializzare gli oggetti con Java, serve questo metodo.
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeInt(id.intValue());
        out.writeUTF(from.getValue());
        out.writeUTF(to.getValue());
        out.writeObject(dateSent.getValue());
        out.writeUTF(subject.getValue());
        out.writeUTF(text.getValue());
    }

    // E per deserializzarli, serve questo.
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.id = new SimpleIntegerProperty(in.readInt());
        this.from = new SimpleStringProperty(in.readUTF());
        this.to = new SimpleStringProperty(in.readUTF());
        this.dateSent = new SimpleObjectProperty<>((Date) in.readObject());
        this.subject = new SimpleStringProperty(in.readUTF());
        this.text = new SimpleStringProperty(in.readUTF());
    }

    @Override
    public int compareTo(Email o) {
        return Integer.compare(o.getId(), this.getId());
    }

    // Per serializzare su file le email invece utilizziamo JSON
    public static class Adapter extends TypeAdapter<Email> {
        @Override
        public void write(JsonWriter jsonWriter, Email email) throws IOException {
            jsonWriter.beginObject();
            jsonWriter.name("id");
            jsonWriter.value(email.getId());
            jsonWriter.name("from");
            jsonWriter.value(email.getFrom());
            jsonWriter.name("to");
            jsonWriter.value(email.getTo());
            jsonWriter.name("subject");
            jsonWriter.value(email.getSubject());
            jsonWriter.name("text");
            jsonWriter.value(email.getText());
            // La data la codifichiamo in ms come long.
            jsonWriter.name("date");
            jsonWriter.value(email.getDateSent().getTime());
            jsonWriter.endObject();
        }

        @Override
        public Email read(JsonReader jsonReader) throws IOException, NumberFormatException {
            Email email = new Email();

            jsonReader.beginObject();

            while (jsonReader.hasNext()) {
                String name = jsonReader.nextName();
                switch (name) {
                    case "id" -> email.id = new SimpleIntegerProperty(jsonReader.nextInt());
                    case "from" -> email.from = new SimpleStringProperty(jsonReader.nextString());
                    case "to" -> email.to = new SimpleStringProperty(jsonReader.nextString());
                    case "subject" -> email.subject = new SimpleStringProperty(jsonReader.nextString());
                    case "text" -> email.text = new SimpleStringProperty(jsonReader.nextString());
                    case "date" -> email.dateSent = new SimpleObjectProperty<>(new Date(jsonReader.nextLong()));
                    default -> jsonReader.skipValue();
                }
            }

            jsonReader.endObject();

            return email;
        }
    }

}
