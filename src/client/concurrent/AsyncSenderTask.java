package client.concurrent;

import client.controllers.MainController;
import data.Email;
import data.networking.answer.EmailExistenceAnswer;
import data.networking.answer.ErrorAnswer;
import data.networking.answer.ServerAnswer;
import data.networking.request.CheckRequest;
import data.networking.request.SendRequest;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import utils.StringUtils.SanitizationResults;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AsyncSenderTask implements Runnable {

    private final ScheduledExecutorService executorService;
    private final Email emailToSend;
    private final Alert sendingAlert;
    private SanitizationResults emailRecipients;

    public AsyncSenderTask(ScheduledExecutorService executorService, Email emailToSend, Alert sendingAlert,
                           SanitizationResults emailRecipients) {
        this.executorService = executorService;
        this.emailToSend = emailToSend;
        this.sendingAlert = sendingAlert;
        this.emailRecipients = emailRecipients;
    }

    @Override
    public void run() {
        Consumer<String> errorConsumer = (message) -> {
            sendingAlert.close();
            new Alert(Alert.AlertType.ERROR, message).show();
        };

        // Prepariamo e inviamo la richiesta al server per sapere se le email inserite nei destinatari esistono.
        CheckRequest existenceRequest = new CheckRequest(emailToSend.getFrom(), emailRecipients.getEmails(true));
        Callable<Optional<ServerAnswer>> asyncExistenceRequest = ConcurrencyHelper.prepareRequest(existenceRequest);

        Future<Optional<ServerAnswer>> futureCheckTask = executorService.submit(asyncExistenceRequest);

        Optional<ServerAnswer> serverAnswer1 = waitForAnswer(futureCheckTask);

        EmailExistenceAnswer existenceAnswer = null;
        try {
            // Le recuperiamo
            existenceAnswer = (EmailExistenceAnswer) serverAnswer1.orElseThrow();
        } catch (NoSuchElementException e) {
            Platform.runLater(() -> errorConsumer.accept("Errore interno. Riprova."));
            e.printStackTrace();
            return;
        }

        System.out.println(existenceAnswer);

        // Se ne troviamo che non esistono
        EmailExistenceAnswer finalExistenceAnswer = existenceAnswer;
        String missingEmails = existenceAnswer.getExistenceResults()
                                              .keySet()
                                              .stream()
                                              .filter(email -> !finalExistenceAnswer.getExistenceResults().get(email))
                                              .collect(Collectors.joining("\n\t"));

        if (!missingEmails.isEmpty()) {
            // Segnaliamo il problema
            Platform.runLater(() -> errorConsumer.accept("Errore: alcune email inserite non esistono:\n" + missingEmails));
            return;
        }

        // Se siamo qui, tutte le email che sono state inserite esistono: possiamo inviare la mail!
        Callable<Optional<ServerAnswer>> sendRequest = ConcurrencyHelper.prepareRequest(new SendRequest(emailToSend));
        Future<Optional<ServerAnswer>> futureSendTask = executorService.submit(sendRequest);

        Optional<ServerAnswer> serverAnswer2 = waitForAnswer(futureSendTask);

        ServerAnswer sendAnswer = null;
        try {
            sendAnswer = serverAnswer2.orElseThrow();
        } catch (NoSuchElementException e) {
            Platform.runLater(() -> errorConsumer.accept("Errore interno. Riprova."));
            e.printStackTrace();
            return;
        }

        if (sendAnswer.getType() == ServerAnswer.Type.SEND_RESULT) {
            Platform.runLater(() -> {
                MainController.getInstance().closeEmailTab(emailToSend, false);
                sendingAlert.close();
                new Alert(Alert.AlertType.INFORMATION, "Email inviata con successo").show();
            });
        } else {
            ErrorAnswer answer = (ErrorAnswer) sendAnswer;
            Platform.runLater(() -> errorConsumer.accept(answer.getStatusMessage()));
        }

    }

    private Optional<ServerAnswer> waitForAnswer(Future<Optional<ServerAnswer>> optionalFuture) {
        Consumer<String> errorConsumer = (message) -> {
            sendingAlert.close();
            new Alert(Alert.AlertType.ERROR, message).show();
        };

        return ConcurrencyHelper.waitForAnswer(5, optionalFuture,
                                               "[AsyncSenderTask]",
                                               errorConsumer, errorConsumer);
    }


}
