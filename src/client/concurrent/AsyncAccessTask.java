package client.concurrent;

import client.MailClient;
import client.Model;
import client.controllers.AccessScreenController;
import data.networking.answer.EmailExistenceAnswer;
import data.networking.answer.ErrorAnswer;
import data.networking.answer.NewEmailsAnswer;
import data.networking.answer.ServerAnswer;
import data.networking.request.AuthRequest;
import data.networking.request.CheckRequest;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class AsyncAccessTask implements Runnable {
    private final ScheduledExecutorService executorService;
    private final AccessScreenController controller;

    public AsyncAccessTask(ScheduledExecutorService executorService, AccessScreenController controller) {
        this.executorService = executorService;
        this.controller = controller;
    }

    @Override
    public void run() {

        // Recuperiamo la mail scritta nel field apposito.
        String emailName;
        try {
            FutureTask<String> emailGetter = new FutureTask<>(controller::getEmailFieldText);
            Platform.runLater(emailGetter);
            emailName = emailGetter.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        // Accodo la richiesta da inviare al server per sapere se l'email inserita è già registrata nel server.
        // Viene usata una Optional per il risultato perchè permette di rendere più esplicito il fatto che
        // potenzialmente la richiesta non restituisce alcuna risposta (in caso di errore).
        Callable<Optional<ServerAnswer>> asyncCheck = ConcurrencyHelper.prepareRequest(new CheckRequest(emailName));
        Future<Optional<ServerAnswer>> futureServerAnswer = executorService.submit(asyncCheck);

        // Se viene lanciata una NoSuchElementException, significa che qualsiasi errore sia capitato all'interno di
        // waitForAnswer è stato già gestito. L'unica cosa che manca da fare è terminare in anticipo il flow della
        // richiesta di accesso che non può essere effettuata.
        ServerAnswer serverAnswer;
        try {
            serverAnswer = waitForAnswer(5, futureServerAnswer).orElseThrow();
        } catch (NoSuchElementException e) {
            return;
        }

        // In funzione del tipo di risposta che il server ci consegna, dobbiamo comportarci diversamente.
        ServerAnswer.Type type = serverAnswer.getType();
        if (type == ServerAnswer.Type.ERROR) {
            // Per gli errori, dobbiamo solo mostrare che è avvenuto e riabilitare l'interfaccia
            ErrorAnswer finalServerAnswer = (ErrorAnswer) serverAnswer;
            Platform.runLater(() -> {
                controller.showMessage(String.format("Errore: %s", finalServerAnswer.getStatusMessage()));
                controller.toggleInterface(true);
            });
        } else if (type == ServerAnswer.Type.EMAIL_EXISTENCE) {
            EmailExistenceAnswer answer = (EmailExistenceAnswer) serverAnswer;
            System.out.println(serverAnswer);

            boolean emailExists = answer.getExistenceResult(emailName);
            Platform.runLater(() -> {
                controller.showMessage(String.format("La mail %s", emailExists ? "esiste" : "non esiste"));
                controller.toggleInterface(true);
            });

            Optional<ButtonType> choice = Optional.empty();

            // Se la mail non è stata registrata, prepariamo il prompt per richiedere se lo si vuole fare ora
            if (!emailExists) {

                // Prepariamo la task da far lanciare per mostrare il dialogo.
                // Tutto deve essere preparato nella task ed eseguito dalla Platform, altrimenti viene lanciata una
                // IllegalStateException.
                FutureTask<Optional<ButtonType>> dialog = new FutureTask<>(() -> {
                    // Prepariamo un prompt per sapere se, visto che la mail non esiste, la si voglia registrare.
                    Alert emailCreationAlert = new Alert(Alert.AlertType.CONFIRMATION,
                                                         "La mail che hai inserito (\"" + emailName + "\") non è mai " +
                                                         "stata registrata. Vuoi farlo ora?",
                                                         ButtonType.YES, ButtonType.CANCEL);
                    // Con questa riga l'Alert viene reso ridimensionabile in automatico per rendere leggibile tutto
                    // il messaggio
                    emailCreationAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                    // Usato per essere sicuri che l'alert appaia sempre davanti alla finestra principale
                    ((Stage) emailCreationAlert.getDialogPane().getScene().getWindow()).setAlwaysOnTop(true);
                    return emailCreationAlert.showAndWait();
                });

                // Dobbiamo eseguirla nel Thread dell'applicazione FX, altrimenti vengono lanciate eccezioni
                Platform.runLater(dialog);

                try {
                    // Attendiamo col blocco di questo thread
                    choice = dialog.get();
                } catch (InterruptedException | ExecutionException e) {
                    // Se siamo qui, qualcosa di davvero inatteso è avvenuto
                    throw new RuntimeException(e);
                }
            }

            // Se la mail esiste, non viene richiesta alcuna conferma e si parte immediatamente col recupero dei dati
            // dell'utente. Altrimenti, se il pulsante cliccato è il SI, allora proseguiamo comunque con la
            // registrazione server. Viene utilizzato orElse per non lanciare eccezioni per casi strani, già
            // verificati prima.
            if (emailExists || choice.orElse(ButtonType.CANCEL) == ButtonType.YES) {
                Callable<Optional<ServerAnswer>> asyncAuth =
                        ConcurrencyHelper.prepareRequest(new AuthRequest(emailName));
                Future<Optional<ServerAnswer>> futureServerAuthAnswer = executorService.submit(asyncAuth);

                ServerAnswer serverAnswer2;
                try {
                    serverAnswer2 = waitForAnswer(5, futureServerAuthAnswer).orElseThrow();
                } catch (NoSuchElementException e) {
                    return;
                }

                ServerAnswer.Type type2 = serverAnswer2.getType();
                if (type2 == ServerAnswer.Type.ERROR) {
                    // Errore, non facciamo altro eccetto che mostrarlo
                    ErrorAnswer finalServerAnswer = (ErrorAnswer) serverAnswer2;
                    Platform.runLater(() -> {
                        controller.showMessage(String.format("Errore: %s", finalServerAnswer.getStatusMessage()));
                        controller.toggleInterface(true);
                    });
                } else if (type2 == ServerAnswer.Type.NEW_EMAILS) {
                    NewEmailsAnswer emailsAnswer = (NewEmailsAnswer) serverAnswer2;
                    // Successo! recuperiamo le email, aggiungiamole al model e facciamo partire la
                    // sincronizzazione con il server.
                    Platform.runLater(() -> {
                        controller.hideMessage();

                        Model model = Model.getInstance();
                        model.getReceivedEmails()
                             .addAll(emailsAnswer.getEmails());
                        model.setEmailName(emailName);
                        model.startPullRequestService();

                        MailClient client = MailClient.getInstance();
                        client.setMainWindowTitle("Casella di posta di " + emailName);
                        client.hideLoginWindow();
                        client.showMainWindow();
                    });

                } else {
                    Platform.runLater(() -> {
                        controller.showMessage("Si è verificato un errore interno");
                        controller.toggleInterface(true);
                    });
                    throw new RuntimeException("Unhandled ServerAnswer Type: " + serverAnswer.getType());
                }

            } else {
                // Altrimenti semplicemente terminiamo il flow, visto che non ci si vuole registrare.
                Platform.runLater(() -> {
                    controller.hideMessage();
                    controller.toggleInterface(true);
                });
            }

        } else {
            Platform.runLater(() -> {
                controller.showMessage("Si è verificato un errore interno");
                controller.toggleInterface(true);
            });
            throw new RuntimeException("Unhandled ServerAnswer Type: " + serverAnswer.getType());
        }
    }

    /**
     * Wrapper di {@link ConcurrencyHelper#waitForAnswer(long, Future, String, Consumer, Consumer)}
     */
    private Optional<ServerAnswer> waitForAnswer(long timeout, Future<Optional<ServerAnswer>> optionalFuture) {
        Consumer<String> errorConsumer =
                (message) -> {
                    controller.showMessage(message);
                    controller.toggleInterface(true);
                };

        return ConcurrencyHelper.waitForAnswer(timeout, optionalFuture, "[Access Screen Async Service]",
                                               errorConsumer, errorConsumer);
    }

}
