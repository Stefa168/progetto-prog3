package client.concurrent;

import client.MailClient;
import data.networking.answer.ServerAnswer;
import data.networking.request.ClientRequest;
import javafx.application.Platform;
import server.MailServer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class ConcurrencyHelper {
    /**
     * asyncServerRequest restituisce un Callable che esegue in modo asincrono la richiesta specificata.
     *
     * @param request La richiesta da inviare al server in modo asincrono.
     * @return Un Optional con la risposta del server. Se vi sono problemi con la richiesta, l'optional è vuoto.
     */
    public static Callable<Optional<ServerAnswer>> prepareRequest(ClientRequest request) {
        return () -> {
            try (Socket socket = new Socket(MailClient.REMOTE_IP, MailServer.SERVER_PORT);
                 ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {

                // Inviamo la richiesta che ci è stata indicata
                outputStream.writeObject(request);

                // Restituiamo la risposta del server.
                return Optional.of((ServerAnswer) inputStream.readObject());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        };
    }

    /**
     * Attende che un Future termini, entro un certo tempo limite.
     *
     * @param timeout                 Tempo massimo, in secondi, per cui il metodo attende un risultato dal Future
     * @param optionalFuture          Il Future per cui attendere
     * @param callerName              Il nome del servizio chiamante
     * @param errorHandler            Handler per gestire gli errori generici
     * @param connectionTimoutHandler Handler per gestire l'errore di Timeout.
     * @return La risposta dal server
     */
    public static Optional<ServerAnswer> waitForAnswer(long timeout, Future<Optional<ServerAnswer>> optionalFuture,
                                                       String callerName, Consumer<String> errorHandler,
                                                       Consumer<String> connectionTimoutHandler) {
        Optional<ServerAnswer> serverAnswer = Optional.empty();
        try {
            serverAnswer = optionalFuture.get(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println(callerName + " Service interrupted unexpectedly");
        } catch (ExecutionException e) {
            String message;

            if (e.getCause() instanceof ConnectException || e.getCause() instanceof UnknownHostException) {
                message = e.getCause().getMessage();

                if (message.equals("Connection refused: connect")) {
                    message = "il server non risponde o è irraggiungibile";
                }

                // ConnectException
                System.out.println(callerName + " Errore di connessione: " + message);
            } else {
                e.printStackTrace();
                message = e.getMessage();
            }

            final String finalMessage = message;
            Platform.runLater(() -> errorHandler.accept("Errore: " + finalMessage));
        } catch (TimeoutException e) {
            optionalFuture.cancel(true);
            Platform.runLater(() -> connectionTimoutHandler.accept("Errore: il server non risponde o è " +
                                                                           "irraggiungibile"));
        }
        return serverAnswer;
    }
}
