package client.concurrent;

import client.MailClient;
import client.Model;
import client.controllers.MainController;
import data.Email;
import data.networking.answer.NewEmailsAnswer;
import data.networking.request.PullRequest;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import server.MailServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class EmailGetterService implements Runnable {
    private final ScheduledExecutorService executorService;
    private final SimpleStringProperty emailNameProperty;
    private ScheduledFuture<?> currentExecution = null;

    private int failedConnectionTries = 0;

    public EmailGetterService(ScheduledExecutorService executorService, SimpleStringProperty emailNameProperty) {
        this.executorService = executorService;
        this.emailNameProperty = emailNameProperty;
    }

    @Override
    public void run() {
        // Apriamo le risorse necessarie: una connessione remota e i due stream per gli oggetti in entrata e in uscita.
        try (Socket socket = new Socket(MailClient.REMOTE_IP, MailServer.SERVER_PORT);
             ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {

            // Creo un set che codifica tutte gli ID delle email che ho già in locale.
            // Questo è necessario per non creare problemi di reference (tra la lista delle email ricevute e la lista
            // delle email aperte) se invece provassi a cancellare tutte le email dal model per poi inserirle da 0.
            HashSet<Integer> alreadyReceivedEmails = Model.getInstance()
                                                          .getReceivedEmails()
                                                          .stream()
                                                          .map(Email::getId)
                                                          .collect(Collectors.toCollection(HashSet::new));

            // Creo la richiesta e la invio al server.
            PullRequest request = new PullRequest(emailNameProperty.getValue(), alreadyReceivedEmails);
            outputStream.writeObject(request);

            // Attendo una risposta dal server.
            NewEmailsAnswer answer = (NewEmailsAnswer) inputStream.readObject();
            System.out.println(answer);

            int oldTries = resetTries();

            // Preparo una FutureTask da far eseguire alla Platform. In questo modo non vengono lanciate eccezioni
            // (cosa che avverrebbe se invece provassi a modificare direttamente la lista delle email ricevute nel
            // model)
            FutureTask<Void> platformTask = new FutureTask<>(() -> {
                Model.getInstance().getReceivedEmails().addAll(answer.getEmails());
                // Facciamo illuminare la finestra se non è selezionata e abbiamo ricevuto delle email nuove.
                if (answer.getEmails().size() > 0) {
                    // MailClient.getInstance().blinkClientWindow();
                    MainController.getInstance()
                                  .showStatusBarMessage(String.format("Recuperate %d nuove email.", answer.getEmails()
                                                                                                          .size()),
                                                        Color.BLACK);

                } else {
                    // Mostriamo un messaggio che ci informa che siamo riusciti a connetterci al server, ma che
                    // nessuna nuova mail è stata recuperata. Questo if evita di nascondere messaggi più
                    // significativi ad ogni periodo di esecuzione del servizio PULL. Il messaggio sarà mostrato
                    // infatti solo quando siamo usciti da una situazione in cui non potevamo connetterci al server.
                    if (oldTries != 0)
                        MainController.getInstance()
                                      .showStatusBarMessage("Connessione al server ristabilita. Nessuna nuova email " +
                                                            "ricevuta.", Color.BLACK);

                }
                return null;
            });

            Platform.runLater(platformTask);

            // Il motivo per cui viene utilizzata una FutureTask invece di una semplice Runnable è per evitare di
            // cercare di avviare multiple istanze dell'EmailGetterService, cosa che porterebbe a multiple istanze
            // accodate su un esecutore singolo e a possibili race conditions.
            // Con get(), attendiamo indefinitamente che la task termini, prima di programmare una nuova esecuzione
            // del servizio.
            platformTask.get();
        } catch (UnknownHostException | ConnectException e) {
            System.out.println("[GETTER SERVICE]: An error occurred while connecting to the Server: " + e.toString());
            // Mostriamo l'errore
            int tries = getIncrementTries();
            Platform.runLater(() -> {
                // Se è la prima volta che non riusciamo a connetterci, avvisiamo con un warning
                if (tries == 1) {
                    new Alert(Alert.AlertType.WARNING, "Impossibile connettersi al server.\n" +
                                                       "Segue l'errore:\n" + e.getMessage()).show();
                }
                MainController.getInstance()
                              .showStatusBarError(String.format("Fallito %d° tentativo di connessione al server.",
                                                                tries));
            });
        } catch (InterruptedException e) {
            System.out.println("[GETTER SERVICE]: interrupted while waiting for task completion");
        } catch (IOException | ClassNotFoundException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            // Potrebbe essere stata ricevuta una InterruptedException perchè l'applicazione è in chiusura.
            // In quel caso, non dovremmo avviare un'altra esecuzione di questo servizio.
            if (!executorService.isShutdown()) {
                scheduleNextRequest();
            }
        }
    }

    public synchronized void startService() {
        if (currentExecution == null || currentExecution.isDone()) {
            currentExecution = executorService.schedule(this, MailClient.EMAIL_PULL_PERIOD, TimeUnit.SECONDS);
        } else {
            throw new IllegalStateException("Pull Service already started");
        }
    }

    private synchronized void scheduleNextRequest() {
        scheduleNextRequest(MailClient.EMAIL_PULL_PERIOD);
    }

    private synchronized void scheduleNextRequest(long secondsDelay) {
        if (currentExecution != null) {
            currentExecution = executorService.schedule(this, secondsDelay, TimeUnit.SECONDS);
        } else {
            throw new IllegalStateException("Pull Service isn't running");
        }
    }

    public synchronized void stopService() {
        if (currentExecution != null) {
            currentExecution.cancel(true);
        } else {
            throw new IllegalStateException("Pull Service not initialized");
        }
    }

    /**
     * Cerca di posticipare un'esecuzione programmata del servizio.
     *
     * @param secondsDelay Di quanti secondi posticipare la prossima esecuzione
     * @return true se la posticipazione è riuscita, false altrimenti.
     */
    public synchronized boolean pauseService(long secondsDelay) {
        if (currentExecution != null) {
            long remainingTime = currentExecution.getDelay(TimeUnit.SECONDS);
            // Cerchiamo di interrompere o annullare l'esecuzione, se possibile.
            if (currentExecution.cancel(false)) {
                scheduleNextRequest(remainingTime + secondsDelay);
                return true;
            }
            // Se otteniamo false, vuol dire che sta terminando la sua esecuzione.
            return false;
        } else {
            throw new IllegalStateException("Pull Service not initialized");
        }
    }

    private int getIncrementTries() {
        return ++failedConnectionTries;
    }

    private int resetTries() {
        int oldValue = failedConnectionTries;
        failedConnectionTries = 0;
        return oldValue;
    }
}
