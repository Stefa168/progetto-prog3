package client.concurrent;

import client.Model;
import client.controllers.MainController;
import data.Email;
import data.networking.answer.ErrorAnswer;
import data.networking.answer.ServerAnswer;
import data.networking.request.DeleteRequest;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class AsyncDeleteTask implements Runnable {

    private final Set<Email> emailsToDelete;
    private final String owner;
    private final ExecutorService executorService;

    public AsyncDeleteTask(String owner, Set<Email> emailsToDelete, ExecutorService executorService) {
        this.emailsToDelete = emailsToDelete;
        this.owner = owner;
        this.executorService = executorService;
    }

    @Override
    public void run() {
        // Tentiamo di posticipare la prossima esecuzione del PULL service per evitare race conditions.
        FutureTask<Boolean> pullDelayer = new FutureTask<>(() -> Model.getInstance().pausePullRequestService(2));
        Platform.runLater(pullDelayer);

        // Se per caso non siamo riusciti a pausare il servizio, attendiamo un po' di tempo.
        try {
            if (!pullDelayer.get()) {
                Thread.sleep(2000);
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        Callable<Optional<ServerAnswer>> asyncDelete = ConcurrencyHelper.prepareRequest(new DeleteRequest(owner,
                                                                                                          emailsToDelete));
        Future<Optional<ServerAnswer>> futureServerAnswer = executorService.submit(asyncDelete);

        ServerAnswer serverAnswer;
        try {
            serverAnswer = waitForAnswer(5, futureServerAnswer).orElseThrow();
        } catch (NoSuchElementException e) {
            return;
        }

        if (serverAnswer.getType() == ServerAnswer.Type.ERROR) {
            ErrorAnswer finalServerAnswer = (ErrorAnswer) serverAnswer;
            Platform.runLater(() -> {
                new Alert(Alert.AlertType.WARNING, String.format("Errore durante la cancellazione delle mail: %s",
                                                                 finalServerAnswer.getStatusMessage())).show();
            });
        } else if (serverAnswer.getType() == ServerAnswer.Type.DELETE) {
            System.out.println(serverAnswer);
            // Facciamo finalmente cancellare le email al client!
            Platform.runLater(() -> {
                MainController.getInstance().showStatusBarMessage("Email eliminate con successo", Color.GREEN);
                emailsToDelete.forEach(MainController.getInstance()::deleteEmail);
            });
        } else {
            Platform.runLater(() -> {
                new Alert(Alert.AlertType.WARNING, "Si è verificato un errore interno durante la cancellazione delle " +
                                                   "mail").show();
            });
            throw new RuntimeException("Unhandled ServerAnswer Type: " + serverAnswer.getType());
        }
    }

    /**
     * Wrapper di {@link ConcurrencyHelper#waitForAnswer(long, Future, String, Consumer, Consumer)}
     */
    private Optional<ServerAnswer> waitForAnswer(long timeout, Future<Optional<ServerAnswer>> optionalFuture) {
        Consumer<String> errorConsumer =
                (message) -> {
                    new Alert(Alert.AlertType.WARNING, "Si è verificato un problema durante la cancellazione delle " +
                                                       "email:\n" + message).show();
                };

        return ConcurrencyHelper.waitForAnswer(timeout, optionalFuture, "[Delete Task]",
                                               errorConsumer, errorConsumer);
    }
}
