package client.controllers;

import data.Email;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Tab;

public abstract class EmailTabController {
    @FXML
    protected Tab rootTab;

    @FXML
    protected Node subjectNode;
    @FXML
    protected Node fromNode;
    @FXML
    protected Node toNode;
    @FXML
    protected Node textNode;

    protected Email email = null;

    public Tab rootTabNode() {
        return rootTab;
    }

    public void initialize(Email email) {
        if (this.email == null) {
            this.email = email;
        } else {
            throw new RuntimeException("Email Tab Controller Email cannot be set more than once");
        }
    }

    public Email getEmail() {
        return email;
    }

    public abstract boolean isDraft();
}