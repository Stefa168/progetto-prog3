package client.controllers;

import client.MailClient;
import client.Model;
import client.concurrent.AsyncDeleteTask;
import data.Email;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.BiConsumer;

import static javafx.scene.control.Alert.AlertType;

public class MainController implements Initializable {
    private static MainController singleton;

    /*
     * Queste due liste sono riferite anche dal file .fxml collegato a questo controller.
     * Devono ancora essere bindate e ciò viene effettuato in initModel()
     */
    @FXML
    public ListView<Email> receivedEmailsList;
    @FXML
    public ListView<String> contactsList;
    @FXML
    public TabPane emailTabPane;
    @FXML
    public Menu emailMenu;
    @FXML
    public Label statusLabel;
    // Questa HashMap serve per tenere traccia rapidamente di quali email sono state aperte.
    // Potrebbe essere necessario solo un HashSet.
    private final HashMap<Email, EmailTabController> openEmails = new HashMap<>();

    private Model modelSingleton;
    private URL receivedEmailTabStructure;
    private URL draftEmailTabStructure;

    // Il costruttore dovrebbe essere chiamato una sola volta quando il programma parte, dall'FXMLLoader.
    public MainController() {
        setSingleton(this);
    }

    public static MainController getInstance() {
        return singleton;
    }

    // Impostare l'istanza deve essere effettuato da un solo Controller alla volta, in modo da far fallire l'operaizone
    // se per caso più di un controller viene creato.
    private static synchronized void setSingleton(MainController reference) {
        if (singleton == null) {
            singleton = reference;
        } else {
            throw new RuntimeException("Cannot initialize more than one MainController.");
        }
    }

    // Il metodo viene lanciato in automatico dopo che il loader lega le proprietà @FXML al Controller.
    // Non è necessario (e anzi sbagliato) inizializzarlo più di una volta.
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.modelSingleton = Model.getInstance();

        receivedEmailTabStructure = getClass().getResource("../structure/receivedEmail.fxml");
        draftEmailTabStructure = getClass().getResource("../structure/newEmail.fxml");

        receivedEmailsList.setCellFactory(listView -> new EmailListCell());

        // Informo questa ListView di quale ObservableList deve occuparsi e monitorare.
        receivedEmailsList.setItems(modelSingleton.getReceivedEmails().sorted());

        // Per un bug non è possibile impostare la SelectionMode da FXML.
        receivedEmailsList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    @FXML
    private void handleEmailListMouseClick(MouseEvent event) {
        // Azione di apertura di una singola email
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            if (event.getClickCount() == 2) {
                Email selectedEmail = receivedEmailsList.getSelectionModel().getSelectedItem();
                Tab newTab = openReceivedEmailTab(selectedEmail);
                if (newTab != null)
                    setCurrentTab(newTab);
            }
        }
    }

    @FXML
    private void handleEmailListKeyboardTyped(KeyEvent event) {
        // Viene usato KeyCodeCombination perchè solo matchare il pulsante premuto con il KeyEvent non permette di
        // distinguere tra combinazioni diverse in cui vengono utilizzati i tasti modificatori.
        // Azione di apertura delle email selezionate, premendo invio.
        if (new KeyCodeCombination(KeyCode.ENTER).match(event)) {
            for (Email selectedEmail : receivedEmailsList.getSelectionModel().getSelectedItems()) {
                Tab newTab = openReceivedEmailTab(selectedEmail);
                if (newTab != null)
                    setCurrentTab(newTab);
            }
        } else if (new KeyCodeCombination(KeyCode.DELETE).match(event)) {
            // E' necessario salvare le specifiche reference per non incappare in eccezioni NoSuchElementException.
            Set<Email> selectedEmails = Set.copyOf(receivedEmailsList.getSelectionModel().getSelectedItems());
            MailClient.getInstance()
                      .getExecutorService()
                      .submit(new AsyncDeleteTask(Model.getInstance()
                                                       .getEmailName(),
                                                  selectedEmails,
                                                  MailClient.getInstance().getExecutorService()));
        }
    }

    public void showStatusBarError(String message) {
        showStatusBarMessage(message, Color.RED);
    }

    public void showStatusBarMessage(String message, Color textColor) {
        Date d = new Date();
        statusLabel.setText(String.format("%tH:%tM : %s", d, d, message));
        statusLabel.setTextFill(textColor);
    }

    public void clearStatusBarMessage() {
        statusLabel.setText("");
    }

    @FXML
    private Tab openNewEmailTab() {
        Email newEmail = new Email(-1, "from@me.com", "", "Nuova Email", "", new Date());
        Tab newEmailTab = openEmailTab(newEmail, draftEmailTabStructure, this::closeEmailTab);
        setCurrentTab(newEmailTab);

        return newEmailTab;
    }

    public Tab openNewEmailTab(Email email) {
        return openEmailTab(email, draftEmailTabStructure, this::closeEmailTab);
    }

    private Tab openReceivedEmailTab(Email selectedEmail) {
        if (!openEmails.containsKey(selectedEmail)) {
            return openEmailTab(selectedEmail, receivedEmailTabStructure, this::closeEmailTab);
        } else {
            return null;
        }
    }

    public void setCurrentTab(Tab newCurrent) {
        emailTabPane.getSelectionModel().select(newCurrent);
    }

    private Tab openEmailTab(Email email, URL emailTabStructure, BiConsumer<Email, Event> closerMethod) {
        FXMLLoader tabLoader = new FXMLLoader(emailTabStructure);
        try {
            tabLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        EmailTabController controller = tabLoader.getController();
        controller.initialize(email);

        openEmails.put(email, controller);

        Tab newTab = controller.rootTabNode();

        emailTabPane.getTabs().add(newTab);
        newTab.setOnCloseRequest(event -> closerMethod.accept(email, event));

        return newTab;
    }

    // Il metodo è pubblico per poter essere chiamato dai tab nel caso in cui viene premuto uno dei loro bottoni.
    public void deleteEmail(Email selectedEmail) {
        // Dobbiamo chiudere la tab della mail corrispondente se è aperta.
        closeEmailTab(selectedEmail);
        modelSingleton.getReceivedEmails().remove(selectedEmail);
    }

    private void closeEmailTab(Email selectedEmail, Event event) {
        closeEmailTab(selectedEmail);
        event.consume();
    }

    /**
     * Cerca di chiudere una tab di una email.
     * Se si tratta di una email ricevuta, viene chiusa automaticamente.
     * Se si tratta di una bozza, viene visualizzato un {@link Alert} che chiede conferma per la chiusura.
     *
     * @param selectedEmail la Mail da chiudere
     * @return true se la tab è stata chiusa, false altrimenti
     */
    private boolean closeEmailTab(Email selectedEmail) {
        return closeEmailTab(selectedEmail, true);
    }

    /**
     * Cerca di chiudere una tab di una email.
     * Se si tratta di una email ricevuta, viene chiusa automaticamente.
     * Se si tratta di una bozza, viene visualizzato un {@link Alert} che chiede conferma per la chiusura.
     *
     * @param selectedEmail    la Mail da chiudere
     * @param askBeforeClosing se bisogna visualizzare o meno una conferma per la chiusura della tab.
     * @return true se la tab è stata chiusa, false altrimenti
     */
    public boolean closeEmailTab(Email selectedEmail, boolean askBeforeClosing) {
        if (openEmails.containsKey(selectedEmail)) {
            EmailTabController controller = openEmails.get(selectedEmail);

            Alert alert = null;
            ButtonType yesButton = new ButtonType("Elimina");

            // Potrebbe essere necessario un refactor:
            // La chiusura della tab dovrebbe essere scollegato dall'Alert inviato se la mail è una bozza.
            // Il controller della bozza dovrebbe lanciare un metodo dedicato.
            if (askBeforeClosing && controller.isDraft()) {
                alert = new Alert(AlertType.CONFIRMATION,
                                  "Confermi la chiusura della bozza \"" + selectedEmail.getSubject() + "\"?\n" +
                                  "Non sarà possibile recuperarla, una volta chiusa.",
                                  yesButton, new ButtonType("Annulla"));
                alert.showAndWait();
            }

            if (alert == null || alert.getResult() == yesButton) {
                // Rimuoviamo la Tab dell'email dalle tab visualizzate
                emailTabPane.getTabs().remove(controller.rootTabNode());
                // Rimuoviamo la Mail dall'HashMap delle email aperte
                openEmails.remove(selectedEmail);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Visualizza un Alert di conferma per la chiusura di tutte le email aperte.
     *
     * @param prompt Il testo da visualizzare per la richiesta
     * @return true se tutte le tab sono state chiuse, false altrimenti.
     */
    public boolean closeAllEmailsAsk(String prompt) {
        Alert confirmationAlert = new Alert(AlertType.CONFIRMATION, prompt);
        ButtonType choice = confirmationAlert.showAndWait().orElse(ButtonType.CANCEL);

        // Se l'utente ha accettato, chiudiamo tutte le email.
        if (choice == ButtonType.OK) {
            // Tentiamo di chiudere tutte le email.
            return closeAllEmails();
        } else {
            return false;
        }
    }

    /**
     * Per ogni tab aperta, chiama {@link #closeEmailTab(Email)}, in modo da cercare di chiuderle.
     * Se si tratta di tab bozza, in automatico viene chiesto se le si vuole chiudere.
     *
     * @return true se tutte le tab sono state chiuse, false altrimenti
     */
    public boolean closeAllEmails() {
        // Creo un array per non avere problemi con le modifiche che avvengono in tempo reale sul set delle email.
        Email[] emails = openEmails.keySet().toArray(Email[]::new);
        for (Email email : emails) {
            if (!closeEmailTab(email)) {
                return false;
            }
        }
        return true;
    }

    @FXML
    private void logoutButtonClicked(MouseEvent actionEvent) {
        if (closeAllEmailsAsk("Confermi di voler effettuare il logout?")) {
            // Dobbiamo terminare il servizio pull
            Model.getInstance().stopPullRequestService();
            // Ripulisco il model dalle email locali
            modelSingleton.getReceivedEmails().clear();
            // Ripuliamo il campo della email.
            MailClient.getInstance().showLoginWindow("");
            MailClient.getInstance().hideMainWindow();
        }
    }
}