package client.controllers;

import data.Email;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

import java.io.IOException;

// Questa classe è il controller dedicato alle celle della lista.
public class EmailListCell extends ListCell<Email> {
    @FXML
    private Label subject;
    @FXML
    private Label from;
    @FXML
    private Label text;
    @FXML
    private Parent emailCellContainer;

    private FXMLLoader fxmlLoader;

    @Override
    protected void updateItem(Email email, boolean empty) {
        super.updateItem(email, empty);

        if (empty || email == null) {
            setText(null);
            setGraphic(null);
        } else {
            if (fxmlLoader == null) {
                fxmlLoader = new FXMLLoader(getClass().getResource("../structure/emailListCell.fxml"));
                fxmlLoader.setController(this);

                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            subject.setText(email.getSubject());
            from.setText(email.getFrom());
            text.setText(email.getText());

            setText(null);
            setGraphic(emailCellContainer);
        }
    }
}