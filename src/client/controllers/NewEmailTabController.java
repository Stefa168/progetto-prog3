package client.controllers;

import client.MailClient;
import client.Model;
import client.concurrent.AsyncSenderTask;
import data.Email;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import utils.StringUtils;

import java.util.Date;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;

public class NewEmailTabController extends EmailTabController {
    @Override
    public void initialize(Email email) {
        super.initialize(email);

        rootTab.textProperty().bind(((TextField) subjectNode).textProperty());
        ((TextField) subjectNode).textProperty().bindBidirectional(email.subjectProperty());
        // Leghiamo il nome dell'email che attualmente ha effettuato il login a quello del from delle nuove email.
        email.fromProperty().bindBidirectional(Model.getInstance().emailNameProperty());
        ((Label) fromNode).textProperty().bindBidirectional(email.fromProperty());
        ((TextField) toNode).textProperty().bindBidirectional(email.toProperty());
        ((TextArea) textNode).textProperty().bindBidirectional(email.textProperty());
    }

    @Override
    public boolean isDraft() {
        return true;
    }

    @FXML
    private void handleSendButtonClick(MouseEvent event) {
        System.out.println("Pulsante invio premuto su tab dell'email " + email.getId());

        // Eseguiamo dei controlli preliminari sulle email per essere sicuri che non vi siano problemi.
        // I controlli vengono comunque effettuati anche dal server per sicurezza.
        String errorMessage = null;
        StringUtils.SanitizationResults results = null;

        try {
            results = StringUtils.sanitizeEmailListFromString(email.getTo());
        } catch (IllegalArgumentException e) {
            errorMessage = e.getMessage();
        }

        if (errorMessage == null && email.getTo().isEmpty()) {
            errorMessage = "Il campo dei destinatari deve contenere almeno un indirizzo email.";
        }

        // Se si è verificato un errore, termino dopo aver mostrato la causa all'utente.
        if (errorMessage != null) {
            // errore runtime
            Alert errorAlert = new Alert(Alert.AlertType.ERROR, errorMessage);
            errorAlert.showAndWait();

            return;
        }

        // Aggiunta condizione per soddisfare i warning di IntelliJ
        if (results == null) {
            throw new RuntimeException();
        }

        // Se qualche email non è accettabile, le mostro e termino.
        if (!results.areEmailsGood()) {
            errorMessage = "Una o più email sono malformate:\n";
            errorMessage += results.toString(false);

            Alert errorAlert = new Alert(Alert.AlertType.ERROR, errorMessage);
            errorAlert.showAndWait();

            return;
        }

        // Imposto la data di invio della email ora
        email.setDateSent(new Date());

        // Lanciamo la task di invio; questa bloccherà l'interattività con l'interfaccia.
        ScheduledExecutorService service = MailClient.getInstance().getExecutorService();

        // Alert mostrato per bloccare l'interfaccia dell'utente durante l'invio.
        Alert sendingAlert = new Alert(Alert.AlertType.INFORMATION, "Invio in corso...", ButtonType.CANCEL);

        FutureTask<Void> sender = new FutureTask<>(new AsyncSenderTask(service, email, sendingAlert, results), null);

        // Se il prompt viene chiuso, allora cerchiamo di annullare la richiesta di invio.
        sendingAlert.setOnCloseRequest(closeEvent -> sender.cancel(true));

        service.submit(sender);
        sendingAlert.show();
    }

    @FXML
    private void handleSaveButtonClick(MouseEvent event) {
        System.out.println("Pulsante salvataggio premuto su tab dell'email " + email.getId());
    }

    @FXML
    private void handleDeleteButtonClick(MouseEvent event) {
        MainController.getInstance().deleteEmail(email);
    }
}