package client.controllers;

import client.concurrent.AsyncAccessTask;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.util.concurrent.ScheduledExecutorService;

public class AccessScreenController {

    private ScheduledExecutorService executorService;
    @FXML
    private Label statusLabel;
    @FXML
    private TextField emailField;
    @FXML
    private Button accessButton;

    public String getEmailFieldText() {
        return emailField.getText();
    }

    public void setEmailFieldText(String email) {
        emailField.setText(email);
    }

    public void initialize(ScheduledExecutorService executorService) {
        this.executorService = executorService;
    }

    public void toggleInterface(boolean enabled) {
        emailField.setDisable(!enabled);
        accessButton.setDisable(!enabled);
    }

    public void showMessage(String message) {
        if (message != null && !message.isEmpty())
            statusLabel.setText(message);
        statusLabel.setVisible(true);
    }

    public void hideMessage() {
        statusLabel.setVisible(false);
    }

    public void accessButtonClicked(MouseEvent event) {
        toggleInterface(false);

        showMessage("Tentativo di connessione...");

        executorService.submit(new AsyncAccessTask(executorService, this));
    }


}
