package client.controllers;

import client.MailClient;
import client.Model;
import client.concurrent.AsyncDeleteTask;
import data.Email;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import utils.StringUtils;

import java.util.Date;
import java.util.Set;

public class ReceivedEmailTabController extends EmailTabController {
    private static final String replyFormat = """

            ____________________
            Da: %s
            %s
            A: %s
            Oggetto: %s

            %s
            """;

    @FXML
    private Label dateSent;

    public void initialize(Email email) {
        super.initialize(email);

        // Le label vengono bindate debolmente, ma basterebbe impostarne il testo.
        rootTab.textProperty().bind(email.subjectProperty());
        ((Label) fromNode).textProperty().bind(email.fromProperty());
        ((Label) subjectNode).textProperty().bind(email.subjectProperty());
        ((Label) toNode).textProperty().bind(email.toProperty());
        ((Label) textNode).textProperty().bind(email.textProperty());
        dateSent.setText(Email.dateFormat.format(email.getDateSent()));
    }

    private String incorporatedEmailFormat() {
        return String.format(replyFormat,
                             email.getFrom(),
                             email.formattedDate(),
                             email.getFrom(),
                             email.getSubject(),
                             email.getText());
    }

    @Override
    public boolean isDraft() {
        return false;
    }

    @FXML
    private void handleDeleteButtonClick(MouseEvent event) {
        MailClient.getInstance()
                  .getExecutorService()
                  .submit(new AsyncDeleteTask(Model.getInstance()
                                                   .getEmailName(),
                                              Set.of(email),
                                              MailClient.getInstance().getExecutorService()));
    }

    @FXML
    private void handleReplyButtonClick(MouseEvent event) {
        // Creiamo la nuova mail da usare per la tab
        Email replyEmail = new Email(-1,
                                     Model.getInstance().getEmailName(),
                                     email.getFrom(),
                                     "Re: " + email.getSubject(),
                                     incorporatedEmailFormat(),
                                     new Date());

        Tab replyTab = MainController.getInstance().openNewEmailTab(replyEmail);
        // Rendiamo come tab corrente la scheda appena creata.
        MainController.getInstance().setCurrentTab(replyTab);
    }

    @FXML
    private void handleForwardButtonClick(MouseEvent event) {
        Email forwardEmail = new Email(-1,
                                       Model.getInstance().getEmailName(),
                                       "",
                                       "Fwd: " + email.getSubject(),
                                       incorporatedEmailFormat(),
                                       new Date());

        Tab forwardTab = MainController.getInstance().openNewEmailTab(forwardEmail);
        MainController.getInstance().setCurrentTab(forwardTab);
    }

    @FXML
    private void handleReplyAllButtonClick(MouseEvent mouseEvent) {
        Set<String> recipients = StringUtils.sanitizeEmailListFromString(email.getTo())
                                            .getEmails(true);

        String localEmail = Model.getInstance().getEmailName();

        recipients.remove(localEmail);

        // Aggiungiamo l'email del mittente ai riceventi solo se non siamo noi il mittente.
        if (!email.getFrom().equals(localEmail))
            recipients.add(email.getFrom());

        Email replyEmail = new Email(-1,
                                     Model.getInstance().getEmailName(),
                                     String.join(", ", recipients),
                                     "Re: " + email.getSubject(),
                                     incorporatedEmailFormat(),
                                     new Date());

        Tab replyTab = MainController.getInstance().openNewEmailTab(replyEmail);
        MainController.getInstance().setCurrentTab(replyTab);
    }
}
