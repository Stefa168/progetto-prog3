package client;

import client.concurrent.EmailGetterService;
import client.controllers.AccessScreenController;
import client.controllers.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

public class MailClient extends Application {

    public static final String REMOTE_IP = "localhost";
    public static int EMAIL_PULL_PERIOD = 5;
    private static MailClient instance;
    private ScheduledExecutorService executorService;
    private EmailGetterService emailGetterService;
    private Stage loginWindowStage;
    private AccessScreenController accessScreenController;
    private Stage mainWindowStage;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @param stage Stage principale (Root) da passare per impostare dimensioni minime per la finestra.
     * @see
     * <a href="https://stackoverflow.com/questions/45905053/how-can-i-prevent-a-window-from-being-too-small-in-javafx">Stackoverflow</a>
     */
    private static void applyRootSizeConstraints(final Stage stage, boolean setMaxBounds) {
        final Parent root = stage.getScene().getRoot();
        stage.sizeToScene();
        final double deltaWidth = stage.getWidth() - root.getLayoutBounds().getWidth();
        final double deltaHeight = stage.getHeight() - root.getLayoutBounds().getHeight();
        //        System.out.println(deltaWidth + " " + deltaHeight);
        final Bounds minBounds = getBounds(root, Node::minWidth, Node::minHeight);
        stage.setMinWidth(minBounds.getWidth() + deltaWidth);
        stage.setMinHeight(minBounds.getHeight() + deltaHeight);
        final Bounds prefBounds = getBounds(root, Node::prefWidth, Node::prefHeight);
        stage.setWidth(prefBounds.getWidth() + deltaWidth);
        stage.setHeight(prefBounds.getHeight() + deltaHeight);
        if (setMaxBounds) {
            final Bounds maxBounds = getBounds(root, Node::maxWidth, Node::maxHeight);
            stage.setMaxWidth(maxBounds.getWidth() + deltaWidth);
            stage.setMaxHeight(maxBounds.getHeight() + deltaHeight);
        }
    }

    private static Bounds getBounds(final Node node,
                                    final BiFunction<Node, Double, Double> widthFunction,
                                    final BiFunction<Node, Double, Double> heightFunction) {
        final Orientation bias = node.getContentBias();
        double prefWidth;
        double prefHeight;
        if (bias == Orientation.HORIZONTAL) {
            prefWidth = widthFunction.apply(node, (double) -1);
            prefHeight = heightFunction.apply(node, prefWidth);
        } else if (bias == Orientation.VERTICAL) {
            prefHeight = heightFunction.apply(node, (double) -1);
            prefWidth = widthFunction.apply(node, prefHeight);
        } else {
            prefWidth = widthFunction.apply(node, (double) -1);
            prefHeight = heightFunction.apply(node, (double) -1);
        }
        return new BoundingBox(0, 0, prefWidth, prefHeight);
    }

    public static MailClient getInstance() {
        return instance;
    }

    public ScheduledExecutorService getExecutorService() {
        return executorService;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        instance = this;

        Model model = new Model();

        // Thread timed (per quando serve) che gestisce tutti i Callable/Runnable/Thread del client.
        executorService = Executors.newScheduledThreadPool(5);
        emailGetterService = new EmailGetterService(executorService, model.emailNameProperty());

        model.initialize(executorService, emailGetterService);

        loginWindowStage = initLoginWindow();
        mainWindowStage = initMainWindow();

        showLoginWindow(getParameters().getNamed().get("startEmail"));
    }

    /**
     * Inizializza la finestra principale del client email.
     *
     * @return Lo stage della finestra del client.
     */
    public Stage initMainWindow() throws IOException {
        Stage stage = new Stage();

        // Recuperiamo l'elemento radice della schermata.
        FXMLLoader rootLoader = new FXMLLoader(getClass().getResource("structure/mainScreen.fxml"));
        Scene primaryScene = new Scene(rootLoader.load());

        MainController mainController = rootLoader.getController();

        // Dichiaro cosa deve succedere se chiudo la finestra principale.
        stage.setOnCloseRequest((event) -> {
            if (!mainController.closeAllEmailsAsk("Vuoi davvero chiudere il Client di posta elettronica?")) {
                // Se qualche email non è stata chiusa, consumo l'evento per evitare che le bozze siano perse.
                event.consume();
            } else {
                Model.getInstance().stopPullRequestService();
            }
        });

        stage.getIcons().add(new Image(MailClient.class.getResourceAsStream("client-icon.png")));

        // È essenziale eseguire le quattro istruzioni che seguono in questo preciso ordine!!!
        // Impostiamo allo stage come scena la root dell'FXML
        stage.setScene(primaryScene); //1
        stage.sizeToScene(); //2
        // Imponiamo come dimensione minima per la finestra le dimensioni impostate nel file FXML.
        applyRootSizeConstraints(stage, false); //4

        return stage;
    }

    /**
     * Visualizza la finestra principale.
     */
    public void showMainWindow() {
        mainWindowStage.show();
        MainController.getInstance().clearStatusBarMessage();
    }

    /**
     * Nasconde la finestra principale.
     */
    public void hideMainWindow() {
        mainWindowStage.hide();
    }

    /**
     * Imposta il titolo della finestra principale
     *
     * @param title Il titolo da utilizzare
     */
    public void setMainWindowTitle(String title) {
        mainWindowStage.setTitle(title);
    }

    /**
     * Inizializza la finestra di accesso al client email.
     *
     * @return Lo stage della finestra di accesso
     */
    public Stage initLoginWindow() throws IOException {
        Stage stage = new Stage();

        FXMLLoader accessScreenLoader = new FXMLLoader(getClass().getResource("structure/accessScreen.fxml"));
        Scene accessScene = new Scene(accessScreenLoader.load());

        accessScreenController = accessScreenLoader.getController();
        accessScreenController.initialize(executorService);

        stage.setScene(accessScene);
        stage.sizeToScene();
        stage.setResizable(false);
        stage.initStyle(StageStyle.UTILITY);
        return stage;
    }

    /**
     * Visualizza la schermata di login.
     */
    public void showLoginWindow(String email) {
        loginWindowStage.show();
        loginWindowStage.setAlwaysOnTop(true);
        accessScreenController.setEmailFieldText(email);
    }

    /**
     * Nasconde la schermata di login.
     */
    public void hideLoginWindow() {
        loginWindowStage.setAlwaysOnTop(false);
        loginWindowStage.hide();
    }

    // Il metodo viene chiamato prima della chiusura dell'applicazione ma dopo che l'ultima finestra viene chiusa.
    // Il metodo cerca di chiudere gentilmente il thread per le richieste pull, ma se non ci riesce lo termina comunque.
    @Override
    public void stop() throws Exception {
        super.stop();

        executorService.shutdown();
        // verificare se bisogna chiudere prima di aspettare o vice-versa.
        try {
            if (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
                System.out.println("The ExecutorService didn't stop in time. Terminating.\nHasta la Vista, Baby.");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void blinkClientWindow() {
        mainWindowStage.toFront();
    }
}