package client;

import client.concurrent.EmailGetterService;
import data.Email;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.concurrent.ScheduledExecutorService;

public class Model {
    private static Model singleton;
    private final ObservableList<Email> receivedEmails = FXCollections.observableArrayList();
    private final SimpleStringProperty emailName = new SimpleStringProperty();
    private ScheduledExecutorService executorService = null;
    private EmailGetterService emailGetterService = null;

    public Model() {
        if (singleton != null) {
            throw new IllegalStateException("Model has already been instantiated");
        }
        singleton = this;
    }

    public static synchronized Model getInstance() {
        if (singleton == null) {
            throw new IllegalStateException("Model hasn't been initialized yet.");
        }
        return singleton;
    }

    public String getEmailName() {
        return emailName.get();
    }

    public void setEmailName(String emailName) {
        this.emailName.set(emailName);
    }

    public SimpleStringProperty emailNameProperty() {
        return emailName;
    }

    public synchronized void initialize(ScheduledExecutorService executorService,
                                        EmailGetterService emailGetterService) {
        if (this.executorService != null || this.emailGetterService != null) {
            throw new IllegalStateException("Model has already been initialized");
        }
        this.executorService = executorService;
        this.emailGetterService = emailGetterService;
    }

    public ObservableList<Email> getReceivedEmails() {
        return receivedEmails;
    }

    public synchronized void startPullRequestService() {
        emailGetterService.startService();
    }

    public synchronized void stopPullRequestService() {
        emailGetterService.stopService();
    }

    public synchronized boolean pausePullRequestService(long secondsDelay) {
        return emailGetterService.pauseService(secondsDelay);
    }
}
